#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>
using namespace std;

// структура для флюгера (направление и скорость ветра)
struct WindData {
    double windSpeed;
    double windDirection;

    WindData() : windSpeed(0), windDirection(0) {}

    WindData(double w, double wd)
            : windSpeed((w >= 0 && w <= 300) ? w : 0),
              windDirection((wd >= 0 && wd <= 360) ? wd : 0) {}

    WindData(const WindData& other)
            : windSpeed(other.windSpeed), windDirection(other.windDirection) {}
};

// структура для метеостанции (температура, давление, направление и скорость ветра)
struct WeatherData {
    double temperature;
    double windSpeed;
    double windDirection;
    double pressure;

    WeatherData() : temperature(0), windSpeed(0), windDirection(0), pressure(0) {}

    WeatherData(double t, double w, double wd, double p)
            : temperature((t >= -100 && t <= 100) ? t : 0),
              windSpeed((w >= 0 && w <= 100) ? w : 0),
              windDirection((wd >= 0 && wd <= 360) ? wd : 0),
              pressure((p >= 700 && p <= 1080) ? p : 0) {}

    WeatherData(const WeatherData& other)
            : temperature(other.temperature), windSpeed(other.windSpeed),
              windDirection(other.windDirection), pressure(other.pressure) {}
};

// создание базового класса для всех устройств
class Device {
public:
    virtual ~Device() {} // утилизация устройства
    virtual string identify() const = 0; // определение устройства
};

// флюгер
class Fluger : public Device {
public:
    Fluger(double x, double y, WindData data) : x(x), y(y), data(data) {}
    Fluger(const Fluger& other)
            : x(other.x), y(other.y), data(other.data) {}

    double getX() const { return x; }
    double getY() const { return y; }
    WindData getWindData() const { return data; }
    string identify() const { return "Fluger"; }

private:
    double x, y;
    WindData data;
};

// метеостанция
class Station : public Device {
public:
    Station(double x, double y, WeatherData data) : x(x), y(y), data(data) {}
    Station(const Station& other)
            : x(other.x), y(other.y), data(other.data) {}

    double getX() const { return x; }
    double getY() const { return y; }
    WeatherData getWeatherData() const { return data; }
    string identify() const { return "Station"; }

private:
    double x, y;
    WeatherData data;
};

// метеоцентр
class WeatherCenter {
public:
    WeatherCenter()= default;
    // корректное удаление приборов, которыми он управлял
    ~WeatherCenter() {
        for (auto  &device : devices) {
            delete device;
        }
    }


    // добавление устройства в список "отслеживаемых"
    void addDevice(Device* device) {
        devices.push_back(device);
    }

    // получение значений погоды
    WeatherData getWeatherAtPoint(double x, double y) {
        WeatherData result;
        double totalWeight = 0;

        // проход по всем устроствах и вычисление расстояния до точки
        for (auto device : devices) {
            double dx, dy, distance, weight;

            // если это - метеостанция
            if (Station* station = dynamic_cast<Station*>(device)) {
                dx = station->getX() - x;
                dy = station->getY() - y;
                distance = sqrt(dx * dx + dy * dy);
                if (distance < 1e-9) {
                    return station->getWeatherData();
                }

                weight = 1 / distance;
                totalWeight += weight;
                result.temperature += weight * station->getWeatherData().temperature;
                result.windSpeed += weight * station->getWeatherData().windSpeed;
                result.windDirection += weight * station->getWeatherData().windDirection;
                result.pressure += weight * station->getWeatherData().pressure;
            }
                // если это - флюгер
            else if (Fluger* fluger = dynamic_cast<Fluger*>(device)) {
                dx = fluger->getX() - x;
                dy = fluger->getY() - y;
                distance = sqrt(dx * dx + dy * dy);
                weight = 1 / distance;
                totalWeight += weight;
                result.windSpeed += weight * fluger->getWindData().windSpeed;
                result.windDirection += weight * fluger->getWindData().windDirection;
            }
        }
        if (totalWeight > 0) {
            result.temperature /= totalWeight;
            result.windSpeed /= totalWeight;
            result.windDirection /= totalWeight;
            result.pressure /= totalWeight;
        }

        return result;
    }
private:
    vector<Device*> devices; // список отслеживаемых приборов
};

int main() {
    // инициализация данных
    WeatherData initData(30, 10, 270, 1000);
    Station defaultStation(0, 0, initData);
    Fluger defaultFluger(0, 0, WindData(10, 270));

    // проверка копирования
    WeatherData copyData(initData);
    assert(copyData.temperature == initData.temperature);
    assert(copyData.windSpeed == initData.windSpeed);
    assert(copyData.pressure == initData.pressure);
    WindData initWindData(10, 0);
    WindData copyWindData(initWindData);
    assert(copyWindData.windSpeed == initWindData.windSpeed);
    Fluger copyFluger(defaultFluger);
    assert(copyFluger.getX() == defaultFluger.getX());
    assert(copyFluger.getY() == defaultFluger.getY());
    assert(copyFluger.getWindData().windSpeed == defaultFluger.getWindData().windSpeed);
    Station copyStation(defaultStation);
    assert(copyStation.getX() == defaultStation.getX());
    assert(copyStation.getY() == defaultStation.getY());
    assert(copyStation.getWeatherData().temperature == defaultStation.getWeatherData().temperature);
    assert(copyStation.getWeatherData().windSpeed == defaultStation.getWeatherData().windSpeed);
    assert(defaultStation.getX() == 0.0);
    assert(defaultStation.getY() == 0.0);
    assert(defaultStation.getWeatherData().temperature == 30.0);
    assert(defaultStation.getWeatherData().windSpeed == 10.0);

    // проверка на ожидаемые значения
    Station initializedStation(1, 2, initData);
    WeatherCenter center; // управление разными устройствами
    WeatherData d1(20.5, 5.0, 200, 1000), d2(21.0, 4.5, 150, 1005), d3(19.0, 5.5, 100, 1002);
    WindData d4(6.0, 270);
    // создание отдельных устройств (станции)
    Device* s1_ptr = new Station(0, 0, d1);
    Device* s2_ptr = new Station(1, 0, d2);
    Device* s3_ptr = new Station(0, 1, d3);
    Device* s4_ptr = new Fluger(1, 1, d4);

    // добавляем устройства в центр погоды
    center.addDevice(s1_ptr);
    center.addDevice(s2_ptr);
    center.addDevice(s3_ptr);
    center.addDevice(s4_ptr);

    // данные погоды в точке (0.5, 0.5)
    WeatherData data = center.getWeatherAtPoint(0.5, 0.5);

    // вывод данных
    cout << "Temperature: " << data.temperature << endl;
    cout << "Wind Speed: " << data.windSpeed << endl;
    cout << "Wind direction: " << data.windDirection << endl;
    cout << "Pressure: " << data.pressure << endl;

    assert(data.temperature > 0);
    assert(data.windSpeed > 0);
    assert(data.pressure > 0);

    cout << endl << "All tests passed successfully!" << endl;

    return 0;
}
